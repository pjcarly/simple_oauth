<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the OAuth App Authorization entity.
 *
 * @see \Drupal\simple_oauth\Entity\OAuthAppAuthorization
 */
class OAuthAppAuthorizationAccessControlHandler extends EntityAccessControlHandler
{
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account)
  {
    // Permissions only apply to own entities.
    if (($owner = $entity->{'owner_id'}) && $account->id() != $owner->target_id && !in_array('administrator', $account->getRoles())) {
      return AccessResult::forbidden();
    }
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view own oauth app authorization entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit own oauth app authorization entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete own oauth app authorization entities');
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL)
  {
    return AccessResult::allowedIfHasPermission($account, 'add oauth app authorization entities');
  }
}
