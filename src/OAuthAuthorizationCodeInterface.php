<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining OAuth Apps.
 *
 * @ingroup simple_oauth
 */
interface OAuthAuthorizationCodeInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface
{
  /**
   * @return integer
   */
  public function getCreatedTime(): int;

  /**
   * @return string|null
   */
  public function getCodeChallenge(): ?string;

  /**
   * @return string|null
   */
  public function getCodeChallengeMethod(): ?string;

  /**
   * @return string
   */
  public function getValue(): string;

  /**
   * @return integer
   */
  public function getExpire(): int;
}
