<?php

namespace Drupal\simple_oauth\Exceptions;

class InvalidAccessTokenException extends \Exception
{
}
