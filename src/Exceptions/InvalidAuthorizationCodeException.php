<?php

namespace Drupal\simple_oauth\Exceptions;

class InvalidAuthorizationCodeException extends \Exception
{
}
