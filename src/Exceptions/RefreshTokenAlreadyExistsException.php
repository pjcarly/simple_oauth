<?php

namespace Drupal\simple_oauth\Exceptions;

class RefreshTokenAlreadyExistsException extends \Exception
{
}
