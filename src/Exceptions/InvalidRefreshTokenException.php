<?php

namespace Drupal\simple_oauth\Exceptions;

class InvalidRefreshTokenException extends \Exception
{
}
