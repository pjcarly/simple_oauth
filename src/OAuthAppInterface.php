<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining OAuth Apps.
 *
 * @ingroup simple_oauth
 */
interface OAuthAppInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface
{
  /**
   * @return integer
   */
  public function getCreatedTime(): int;

  /**
   * @return string
   */
  public function getCallbackUrl(): string;

  /**
   * Returns all the scope ids for the app
   *
   * @return string[]
   */
  public function getScopeIds(): array;

  /**
   * Returns the amount of expiration seconds that must be added to now() when generating a new refresh token
   *
   * @return integer
   */
  public function getExpirationInSeconds(): int;

  /**
   * Check whether the app is active
   *
   * @return boolean
   */
  public function isActive(): bool;
}
