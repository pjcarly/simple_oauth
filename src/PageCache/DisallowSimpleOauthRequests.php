<?php

namespace Drupal\simple_oauth\PageCache;

use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\simple_oauth\Services\OAuthServiceInterface;
use Symfony\Component\HttpFoundation\Request;

class DisallowSimpleOauthRequests implements RequestPolicyInterface
{
  private OAuthServiceInterface $oauthService;

  public function __construct(
    OAuthServiceInterface $oauthService
  ) {
    $this->oauthService = $oauthService;
  }

  /**
   * {@inheritdoc}
   */
  public function check(Request $request)
  {
    return $this->oauthService->getTokenFromRequest($request) ? self::DENY : NULL;
  }
}
