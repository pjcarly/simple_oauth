<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of Access Token entities.
 *
 * @ingroup simple_oauth
 */
class AccessTokenListBuilder extends EntityListBuilder
{

  /**
   * {@inheritdoc}
   */
  public function buildHeader()
  {
    $header['type'] = $this->t('Type');
    $header['user'] = $this->t('Auth User');
    $header['owner'] = $this->t('Owner');
    $header['id'] = $this->t('ID');
    $header['name'] = $this->t('Token');
    $header['app'] = $this->t('App');
    $header['resource'] = $this->t('Resource');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity)
  {
    /* @var $entity AccessToken */
    $type = $entity->{'resource'}->target_id == 'authentication' ? t('Refresh Token') : t('Access Token');
    $row['type'] = $type;
    $user = $entity->{'auth_user_id'}->entity;
    $row['user'] = Link::fromTextAndUrl($user->label(), new Url('entity.user.canonical', [
      'user' => $user->id(),
    ]));
    $owner = $entity->{'user_id'}->entity;
    $row['owner'] = Link::fromTextAndUrl($owner->label(), new Url('entity.user.canonical', [
      'user' => $owner->id(),
    ]));
    $row['id'] = $entity->id();
    $row['name'] = Link::fromTextAndUrl($entity->label(), new Url('entity.access_token.edit_form', [
      'access_token' => $entity->id(),
    ]));

    if (!empty($entity->{'oauth_app'}->entity)) {
      $row['app'] = $entity->{'oauth_app'}->entity->label();
    } else {
      $row['app'] = '';
    }


    $row['resource'] = $entity->{'resource'}->entity->label();

    return $row + parent::buildRow($entity);
  }
}
