<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the OAuth Authorization Code entity.
 *
 * @see \Drupal\simple_oauth\Entity\OAuthAuthorizationCode
 */
class OAuthAuthorizationCodeAccessControlHandler extends EntityAccessControlHandler
{
  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account)
  {
    // Permissions only apply to own entities.
    if (in_array('administrator', $account->getRoles())) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL)
  {
    return AccessResult::allowedIfHasPermission($account, 'add oauth authorization code entities');
  }
}
