<?php

namespace Drupal\simple_oauth\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for OAuth Authorization Code entities.
 */
class OAuthAuthorizationCodeViewsData extends EntityViewsData implements EntityViewsDataInterface
{
  /**
   * {@inheritdoc}
   */
  public function getViewsData()
  {
    $data = parent::getViewsData();

    $data['oauth_authorization_code']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('OAuth Authorization Code'),
      'help' => $this->t('The OAuth authorization code.'),
    ];

    return $data;
  }
}
