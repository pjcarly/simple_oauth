<?php

namespace Drupal\simple_oauth\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for OAuth App entities.
 */
class OAuthAppViewsData extends EntityViewsData implements EntityViewsDataInterface
{
  /**
   * {@inheritdoc}
   */
  public function getViewsData()
  {
    $data = parent::getViewsData();

    $data['oauth_app']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('OAuth App'),
      'help' => $this->t('The OAuth App ID.'),
    ];

    return $data;
  }
}
