<?php

namespace Drupal\simple_oauth\Entity;

use Drupal;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\simple_oauth\AccessTokenInterface;
use Drupal\simple_oauth\Exceptions\RefreshTokenAlreadyExistsException;
use Drupal\simple_oauth\Services\OAuthService;
use Drupal\simple_oauth\Services\OAuthServiceInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Access Token entity.
 *
 * @ingroup simple_oauth
 *
 * @ContentEntityType(
 *   id = "access_token",
 *   label = @Translation("Access Token"),
 *   handlers = {
 *     "view_builder" = "Drupal\simple_oauth\AccessTokenViewBuilder",
 *     "list_builder" = "Drupal\simple_oauth\AccessTokenListBuilder",
 *     "views_data" = "Drupal\simple_oauth\Entity\AccessTokenViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\simple_oauth\Entity\Form\AccessTokenForm",
 *       "add" = "Drupal\simple_oauth\Entity\Form\AccessTokenForm",
 *       "edit" = "Drupal\simple_oauth\Entity\Form\AccessTokenForm",
 *       "delete" = "Drupal\simple_oauth\Entity\Form\AccessTokenDeleteForm",
 *     },
 *     "access" = "Drupal\simple_oauth\AccessTokenAccessControlHandler",
 *   },
 *   base_table = "access_token",
 *   admin_permission = "administer AccessToken entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "value",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/people/access_token/{access_token}",
 *     "edit-form" = "/admin/config/people/access_token/{access_token}/edit",
 *     "delete-form" = "/admin/config/people/access_token/{access_token}/delete"
 *   }
 * )
 */
class AccessToken extends ContentEntityBase implements AccessTokenInterface
{
  use EntityChangedTrait;

  public static function preCreate(EntityStorageInterface $storage_controller, array &$values)
  {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => Drupal::currentUser()->id(),
    ];
  }

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
  {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Access Token entity.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Access Token entity.'))
      ->setReadOnly(TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Creator'))
      ->setDescription(t('The user ID of author of the Access Token entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\simple_oauth\Entity\AccessToken::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['auth_user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setDescription(t('The user ID of the user this access token is authenticating.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\simple_oauth\Entity\AccessToken::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setCardinality(1)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setPropertyConstraints('target_id', [
        'OwnOrAdmin' => [
          'permission' => 'administer access token entities',
        ],
      ]);

    $fields['resource'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Resource'))
      ->setDescription(t('The resource for this Access Token.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'access_token_resource')
      ->setSetting('handler', 'default')
      ->setDefaultValue('global')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'region'=> 'hidden',
        'type' => 'entity_reference_label',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 4,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['access_token_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Refresh Token'))
      ->setDescription(t('The Refresh Token to re-create an Access Token.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'access_token')
      ->setSetting('handler', 'default')
      // TODO: Only allow referencing tokens to the auth resource.
      // ->setDefaultValueCallback('Drupal\node\Entity\Node::getCurrentUserId')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['value'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Token'))
      ->setDescription(t('The token value.'))
      ->setSettings([
        'text_processing' => 0,
      ])
      ->setRequired(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['expire'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Expire'))
      ->setDefaultValueCallback(__CLASS__ . '::defaultExpiration')
      ->setDescription(t('The time when the token expires.'))
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 1,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 1,
      ])
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['scopes'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Scopes'))
      ->setDescription(t('The scopes for this App.'))
      ->setSetting('target_type', 'oauth_scope')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'region'=> 'hidden',
        'type' => 'entity_reference',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference',
        'weight' => 4,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['oauth_app'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('App'))
      ->setDescription(t('The App that requested the Access Token.'))
      ->setSetting('target_type', 'oauth_app')
      ->setSetting('handler', 'default')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'weight' => 4
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ip'] = BaseFieldDefinition::create('string')
      ->setLabel(t('IP Address'))
      ->setDescription(t('The IP-address of the requester the moment the token was created or refreshed.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['user_agent'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('User Agent'))
      ->setDescription(t('The User Agent of the requester the moment the token was created or refreshed.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  public function getCreatedTime(): int
  {
    return $this->get('created')->value;
  }

  public function getOwner()
  {
    return $this->get('user_id')->entity;
  }

  public function getOwnerId()
  {
    return $this->get('user_id')->target_id;
  }

  public function setOwnerId($uid)
  {
    $this->set('user_id', $uid);
    return $this;
  }

  public function setOwner(UserInterface $account)
  {
    $this->set('user_id', $account->id());
    return $this;
  }

  public function setIp(?string $value): self
  {
    $this->set('ip', $value);
    return $this;
  }

  public function setUserAgent(?string $value): self
  {
    $this->set('user_agent', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage)
  {
    parent::preSave($storage);
    // Create the token value as a digestion of the values in the token. This
    // will allow us to check integrity of the token later.
    if ($this->get('value')->isEmpty()) {
      $generator = Drupal::getContainer()?->getParameter('simple_oauth.access_token_generator');
      $generator ??= Drupal\simple_oauth\AccessTokenValue::class;

      /** @var \Drupal\simple_oauth\AccessTokenValueInterface $generator */
      $value = $generator::createFromValues($this->getValuesForTokenSeed())->digest();

      $this->set('value', $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE)
  {
    parent::postSave($storage, $update);
    // If this is not a refresh token then create one.
    if (!$this->isRefreshToken()) {
      $this->addRefreshToken();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isRefreshToken(): bool
  {
    return !$this->get('access_token_id')
      ->isEmpty() && $this->get('resource')->target_id === 'authentication';
  }

  /**
   * Adds a refresh token and links it to this entity.
   */
  protected function addRefreshToken()
  {
    /** @var OAuthService $service */
    $service = Drupal::service('simple_oauth.oauth_service');
    try {
      $service->generateRefreshTokenForAccessToken($this);
    } catch (RefreshTokenAlreadyExistsException) {
    }
  }

  /**
   * Returns the values which you want to use to seed the token value generation.
   *
   * @return string[]
   */
  protected function getValuesForTokenSeed(): array
  {
    $keys = ['auth_user_id', 'expire', 'created', 'resource'];

    foreach ($keys as $key) {
      $value = array_values($this->get($key)->getValue()[0]);
      $values[$key] = $value[0];
    }

    $values['random'] = base64_encode(random_bytes(16));

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function hasPermission($permission): bool
  {
    if ($permission === 'refresh access token') {
      // You can only refresh the access token with a refresh token.
      return $this->isRefreshToken();
    }

    $resource = $this->get('resource')->entity;
    $token_permissions = $resource->get('permissions') ?: [];
    // If the selected permission is not included in the list of permissions
    // for the resource attached to the token, then return FALSE.
    return $resource->id() === 'global' || in_array($permission, $token_permissions, TRUE);
  }

  public static function getCurrentUserId(): int
  {
    return Drupal::currentUser()->id();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultExpiration()
  {
    /** @var OAuthServiceInterface $service */
    $service = Drupal::service("simple_oauth.oauth_service");
    $expiration = $service->getDefaultAccessTokenExpirationTime();
    return [Drupal::time()->getRequestTime() + $expiration];
  }
}
