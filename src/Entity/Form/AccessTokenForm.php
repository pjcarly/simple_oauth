<?php

namespace Drupal\simple_oauth\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\simple_oauth\Services\OAuthServiceInterface;

/**
 * Form controller for Access Token edit forms.
 *
 * @ingroup simple_oauth
 */
class AccessTokenForm extends ContentEntityForm
{

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /* @var $entity \Drupal\simple_oauth\Entity\AccessToken */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $form['langcode'] = [
      '#title' => $this->t('Language'),
      '#type' => 'language_select',
      '#default_value' => $entity->getUntranslated()->language()->getId(),
      '#languages' => Language::STATE_ALL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    /** @var OAuthServiceInterface $service */
    $service = \Drupal::service('simple_oauth.oauth_service');

    $entity = $this->entity;
    $service->setRequestValuesOnAccessToken($entity);
    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Access Token.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Access Token.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('user.access_token.collection', ['user' => $entity->{'auth_user_id'}->target_id]);
  }
}
