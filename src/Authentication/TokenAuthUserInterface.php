<?php

namespace Drupal\simple_oauth\Authentication;


use Drupal\simple_oauth\AccessTokenInterface;
use Drupal\user\UserInterface;

interface TokenAuthUserInterface extends \IteratorAggregate, UserInterface
{
  /**
   * The Access Token
   *
   * @return AccessTokenInterface
   */
  public function getToken(): AccessTokenInterface;
}
