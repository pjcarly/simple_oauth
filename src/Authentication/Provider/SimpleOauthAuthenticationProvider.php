<?php

namespace Drupal\simple_oauth\Authentication\Provider;

use Drupal\Core\Authentication\AuthenticationProviderInterface;
use Drupal\simple_oauth\Services\OAuthServiceInterface;
use Symfony\Component\HttpFoundation\Request;

class SimpleOauthAuthenticationProvider implements AuthenticationProviderInterface
{
  private OAuthServiceInterface $oauthService;

  public function __construct(
    OAuthServiceInterface $oauthService
  ) {
    $this->oauthService = $oauthService;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(Request $request)
  {
    // Check for the presence of the token.
    return (bool) $this->oauthService->getTokenFromRequest($request);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(Request $request)
  {
    $token = $this->oauthService->getTokenFromRequest($request);

    if (!$token) {
      return null;
    }

    return $this->oauthService->getUserByTokenValue($token);
  }
}
