<?php

namespace Drupal\simple_oauth\Services;

use Drupal\Core\Session\AccountInterface;
use Drupal\simple_oauth\AccessTokenInterface;
use Drupal\simple_oauth\Authentication\TokenAuthUserInterface;
use Drupal\simple_oauth\Entity\OAuthAuthorizationCode;
use Drupal\simple_oauth\OAuthAppAuthorizationInterface;
use Drupal\simple_oauth\OAuthAppInterface;
use Drupal\simple_oauth\OAuthAuthorizationCodeInterface;
use Drupal\simple_oauth\OAuthScopeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Request;

interface OAuthServiceInterface
{
  /**
   * Extract an Oauth Token from a Request. Returns null if no Token is found
   *
   * @param Request $request
   * @return string|null
   */
  public function getTokenFromRequest(Request $request): ?string;

  /**
   * Finds the User linked to the Token Value
   *
   * @param string $token
   * @return TokenAuthUserInterface|null
   */
  public function getUserByTokenValue(string $token): ?TokenAuthUserInterface;

  /**
   * Find an OAuth Scope by its ID
   *
   * @param string $id
   * @return OAuthScopeInterface|null
   */
  public function getScopeById(string $id): ?OAuthScopeInterface;

  /**
   * Revokes a Token
   *
   * @param string $token
   * @return self
   */
  public function revokeToken(string $token): self;

  /**
   * @return integer
   */
  public function getDefaultAccessTokenExpirationTime(): int;

  /**
   * @return integer
   */
  public function getDefaultRefreshTokenExpirationTime(): int;

  /**
   * @return integer
   */
  public function getAuthorizationCodeExpirationTime(): int;

  /**
   * Removes all expired tokens
   *
   * @return self
   */
  public function removeExpiredTokens(): self;

  /**
   * Removes all expired authorization codes
   *
   * @return self
   */
  public function removeExpiredAuthorizationCodes(): self;

  /**
   * Remove all the access tokens linked to an app
   *
   * @param OAuthAppInterface $app
   * @return self
   */
  public function removeAccessTokensForApp(OAuthAppInterface $app): self;

  /**
   * Removes all the access tokens for a specific user (for example, in case a user gets blocked or deleted)
   *
   * @param UserInterface $user
   * @return self
   */
  public function removeAllTokensForUser(UserInterface $user): self;

  /**
   * Refreshes the provided refresh token, and returns a new access token.
   *
   * @param AccessTokenInterface $refreshToken
   * @return AccessTokenInterface
   */
  public function refreshRefreshToken(AccessTokenInterface $refreshToken): ?AccessTokenInterface;

  /**
   * Generates a new Access Token for the provided User
   *
   * @param UserInterface $user
   * @param OAuthScopeInterface[] $scopes
   * @param OAuthAppInterface|null $app
   * @return AccessTokenInterface
   */
  public function generateAccessTokenForUser(UserInterface $user, array $scopes): AccessTokenInterface;

  /**
   * Generates an access token via the provided authorization code
   *
   * @param OAuthAuthorizationCode $authorizationCode
   * @return AccessTokenInterface
   */
  public function generateAccessTokenByAuthorizationCode(OAuthAuthorizationCode $authorizationCode): AccessTokenInterface;

  /**
   * @param AccessTokenInterface $accessToken
   * @return AccessTokenInterface
   */
  public function generateRefreshTokenForAccessToken(AccessTokenInterface $accessToken): AccessTokenInterface;

  /**
   * Generates an Authorization Code for the provided app/user
   * 
   * @param AccountInterface $user
   * @param OAuthAppInterface $oauthApp
   * @return OAuthAuthorizationCodeInterface
   */
  public function generateAuthorizationCode(AccountInterface $user, OAuthAppInterface $oauthApp): OAuthAuthorizationCodeInterface;

  /**
   * Sets Request specific values on the Access Token
   *
   * @param AccessTokenInterface $accessToken
   * @return self
   */
  public function setRequestValuesOnAccessToken(AccessTokenInterface $accessToken): self;

  /**
   * Searches the app by its client id. The app must be active Returns null if not found
   *
   * @param string $clientId
   * @return OAuthAppInterface|null
   */
  public function getAppByClientId(string $clientId): ?OAuthAppInterface;

  /**
   * Returns all the scopes a certain user authorized for a certain app
   *
   * @param string $userId
   * @param string $appId
   * @return string[] the ids of the scopes
   */
  public function getAuthorizedScopesForAppByUser(string $userId, string $appId): array;

  /**
   * Check if a user authorized app, returns true if all the scopes of the app were authorized by the user
   *
   * @param AccountInterface $user
   * @param OAuthAppInterface $app
   * @return boolean
   */
  public function didUserAuthorizeApp(AccountInterface $user, OAuthAppInterface $app): bool;

  /**
   * Creates a Authorization Code for a specific user for a specific app
   *
   * @param AccountInterface $user
   * @param OAuthAppInterface $app
   * @param string $codeChallenge (optional) PKCE challenge
   * @param string $codeChallengeMethod (optional) PKCE method used to encode challenge
   * @return OAuthAuthorizationCodeInterface
   */
  public function createAuthorizationCodeForUser(
    AccountInterface $user,
    OAuthAppInterface $app,
    ?string $codeChallenge = null,
    ?string $codeChallengeMethod = null
  ): OAuthAuthorizationCodeInterface;

  /**
   * Removes any authorizations a user might have for an app
   *
   * @param OAuthAppInterface $app
   * @param AccountInterface $user
   * @return self
   */
  public function deauthorizeAppForUser(OAuthAppInterface $app, AccountInterface $user): self;

  /**
   * Creates an oauth authorization for the provided user and app
   *
   * @param OAuthAppInterface $app
   * @param AccountInterface $user
   * @return OAuthAppAuthorizationInterface
   */
  public function authorizeAppForUser(OAuthAppInterface $app, AccountInterface $user): OAuthAppAuthorizationInterface;

  /**
   * Removes the access tokens for a spcific app and user
   * 
   * @param OAuthAppInterface $app
   * @param AccountInterface $user
   * @return OAuthAppAuthorizationInterface
   */
  public function removeAccessTokensForSpecificUserAndApp(OAuthAppInterface $app, UserInterface $user): self;
}
