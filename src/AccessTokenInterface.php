<?php

namespace Drupal\simple_oauth;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Access Token entities.
 *
 * @ingroup simple_oauth
 */
interface AccessTokenInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface
{
  /**
   * Checks if the current token allows the provided permission.
   *
   * @param string $permission
   *   The requested permission.
   *
   * @return bool
   *   TRUE if the permission is included. FALSE otherwise.
   */
  public function hasPermission($permission): bool;

  /**
   * Helper function that indicates if a token is a refresh token.
   *
   * @return bool
   */
  public function isRefreshToken(): bool;

  /**
   * @return integer
   */
  public function getCreatedTime(): int;

  /**
   * @param string|null $value
   * @return self
   */
  public function setIp(?string $value): self;

  /**
   * @param string|null $value
   * @return self
   */
  public function setUserAgent(?string $value): self;
}
