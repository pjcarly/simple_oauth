<?php

namespace Drupal\simple_oauth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;

class OAuthAppUserList extends ControllerBase
{
  /**
   * Provide a list of applications.
   */
  public function appList(User $user)
  {
    $entity_type = 'oauth_app';

    $storage = $this
      ->entityTypeManager()
      ->getStorage($entity_type);

    $ids = $storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('owner_id', $user->id())
      ->execute();
    if (empty($ids)) {
      return [
        '#markup' => $this->t('There are no apps for this user.'),
      ];
    }
    $view_controller = $this->entityTypeManager()->getViewBuilder($entity_type);
    $tokens = $storage->loadMultiple($ids);

    return $view_controller->viewMultiple($tokens);
  }
}
