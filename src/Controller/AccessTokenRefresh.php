<?php

namespace Drupal\simple_oauth\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\simple_oauth\AccessTokenInterface;
use Drupal\simple_oauth\Authentication\TokenAuthUserInterface;
use Drupal\simple_oauth\Exceptions\InvalidRefreshTokenException;
use Drupal\simple_oauth\Services\OAuthServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AccessTokenRefresh extends ControllerBase
{
  private OAuthServiceInterface $oauthService;
  private TimeInterface $time;

  public function __construct(
    OAuthServiceInterface $oauthService,
    AccountProxyInterface $current_user,
    EntityTypeManagerInterface $entity_manager,
    TimeInterface $time
  ) {
    $this->oauthService = $oauthService;
    $this->currentUser = $current_user;
    $this->entityManager = $entity_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('simple_oauth.oauth_service'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('datetime.time')
    );
  }

  /**
   * Controller to return the access token when a refresh token is provided.
   *
   * @todo: Get some flood protection for this, since the request is uncacheable
   * because of the expire counter. Also, there has to be some other better way
   * to render JSON. Investigate that too!
   */
  public function refresh(): Response
  {
    /** @var AccountProxyInterface $currentUser */
    $currentUser = $this->currentUser();
    $account = $currentUser->getAccount();
    // If the account is not a token account, then bail.
    if (!$account instanceof TokenAuthUserInterface) {
      // TODO: Set the error headers appropriately.
      return new Response('', 400);
    }

    $refreshToken = $account->getToken();
    if (!$refreshToken) {
      // TODO: Set the error headers appropriately.
      return new Response('', 400);
    }

    try {
      $newAccessToken = $this->oauthService->refreshRefreshToken($refreshToken);

      $response = (new JsonResponse())
        ->setData($this->normalize($newAccessToken));
    } catch (InvalidRefreshTokenException $ex) {
      $response = new Response('', 400);
    }

    return $response;
  }

  /**
   * Serializes the token either using the serializer or manually.
   *
   * @param AccessTokenInterface $token
   *   The token.
   *
   * @return string
   *   The serialized token.
   */
  protected function normalize(AccessTokenInterface $token)
  {
    $storage = $this->entityTypeManager()
      ->getStorage('access_token');
    $ids = $storage
      ->getQuery()
      ->accessCheck(false)
      ->condition('access_token_id', $token->id())
      ->condition('expire', $this->time->getRequestTime(), '>')
      ->condition('resource', 'authentication')
      ->range(0, 1)
      ->execute();
    if (empty($ids)) {
      // TODO: Add appropriate error handling. Maybe throw an exception?
      return [];
    }

    /** @var AccessToken $refresh_token */
    $refresh_token = $storage->load(reset($ids));
    if (!$refresh_token || !$refresh_token->isRefreshToken()) {
      // TODO: Add appropriate error handling. Maybe throw an exception?
      return [];
    }
    return [
      'access_token' => $token->get('value')->value,
      'token_type' => 'Bearer',
      'expires_in' => $token->get('expire')->value - $this->time->getRequestTime(),
      'refresh_token' => $refresh_token->get('value')->value,
    ];
  }
}
