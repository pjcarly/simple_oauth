<?php

/**
 * @file
 * Contains oauth_app.page.inc..
 *
 * Page callback for OAuth App entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for OAuth App templates.
 *
 * Default template: oauth-app.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_oauth_app(array &$variables) {
  // Fetch OAuth App Entity Object.
  $oauthApp = $variables['oauth_app'];

  $variables['attributes']['class'] = empty($variables['attributes']['class']) ? [] : $variables['attributes']['class'];
  $variables['attributes']['class'][] = 'oauth-app';

  // Helpful $content variable for templates.
  foreach (Element::children($oauthApp) as $key) {
    $variables['content'][$key] = $oauthApp[$key];
  }
}
